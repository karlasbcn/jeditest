import React, { Fragment } from 'react'
import styled from 'styled-components'

import GoldImg     from '../images/gold.png'
import DistanceImg from '../images/distance.png'

const ItemContainer = styled.div`
  height: 100px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  width: 50%;
`

const Image = styled.div`
  height: 100px;
  width: 100px;
  background-image: url(${ ({ src }) => src });
  background-size: contain;
  background-repeat: no-repeat;
`

const Text = styled.span`
  font-size: 18px;
`

const Item = ({ img, children }) => (
  <ItemContainer>
    <Image src={ img } />
    <Text>{ children }</Text>
  </ItemContainer>
)

const Match = ({ gold, distance }) => (
  <Fragment>
    <Item img={ GoldImg }>{ gold } kg </Item>
    <Item img={ DistanceImg }>{ distance } km</Item>
  </Fragment>
)

export default Match;