import React  from 'react';
import styled from 'styled-components'

const Text = styled.div`
  font-size: 40px;
  text-align: center;
  margin-top: 0.25em;
  color: ${ ({ lastWinner }) => lastWinner ? (lastWinner === 'a' ? 'blue' : 'red' ) : 'white' };
`

const Winner= ({ lastWinner, players }) => (
  <Text lastWinner={ lastWinner} >
    { (lastWinner ? (players[lastWinner].name + ' won!') : 'tie!').toUpperCase() }
  </Text>
);

export default Winner;