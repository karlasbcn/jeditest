import React, { Fragment } from 'react'
import styled              from 'styled-components'

const Button = styled.button`
  font-family: Lato;
  color: white;
  padding: 0.5em;
  outline: transparent;
  display: block;
  border-radius: 0.25em;
  border: 0;
  cursor: pointer;
  font-weight: bold;
`

const ActionButton = styled(Button)`
  font-size: 36px;
  background-color: orange;
  margin: 0 auto;
`

const ResetButton = styled(Button)`
  font-size: 16px;
  position: absolute;
  right: 1em;
  bottom: 1em;
  background-color: blue;
`

const Buttons = ({ clickAction, clickReset, textAction }) => (
  <Fragment>
    <ActionButton onClick={ clickAction }>{ textAction }</ActionButton>
    <ResetButton onClick={ clickReset }>Change players</ResetButton>
  </Fragment>
)

export default Buttons;