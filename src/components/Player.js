import React, { Fragment } from 'react';
import styled              from 'styled-components'

import VehicleImg          from '../images/space.png';

const setColor = ({ id }) => id === '1' ? 'blue' : 'red';

const Container = styled.div`
  width: 340px;
  & > div{
    width: 100%;
  }
`

const Title = styled.div`
  font-size: 24px;
  color: ${ setColor };
`

const Name = styled.div`
  font-size: 36px;
`

const Specie = styled.div`
  font-size: 16px;
`

const VehicleIcon = styled.div`
  background-image: url(${ VehicleImg });
  width: 88px !important;
  height: 88px;
  background-size: contain;
  margin: 60px auto;  
  margin-bottom: 10px;
`

const VehicleName = styled.div`
  font-size: 16px;
  text-align: center;
`

const MatchData = styled.div`
  color: ${ setColor };
  text-align: center;
  font-size: 20px;
`

const VehicleData = styled.div`
  text-align: center;
  font-size: 20px;
`

const MatchDetail = ({ match, vehicle, id }) => (
  <Fragment>
    <MatchData id={ id }>
      { match.time.toFixed(3) } hours
    </MatchData>
    <MatchData id={ id }>
      { match.trips } trips
    </MatchData>
    <VehicleData>
      Speed: { vehicle.speed } km/h
    </VehicleData>
    <VehicleData>
      Cargo: { vehicle.capacity } kg
    </VehicleData>
  </Fragment>
)

const Player = ({ id, player, lastMatch }) => (
  <Container>
    <Title id={ id }>Player { id }</Title>
    <Name>{ player.name }</Name>
    <Specie>{ player.specie }</Specie>
    <VehicleIcon />
    <VehicleName>{ player.vehicle.name }</VehicleName>
    { lastMatch && <MatchDetail id={ id } match={ lastMatch } vehicle={ player.vehicle } /> }
  </Container>
)

export default Player;