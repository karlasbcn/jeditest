import React  from 'react'
import styled from 'styled-components'

const Container = styled.div`
  font-size: 64px;
  margin: 0 auto;
`

const Points = styled.span`
  color : ${ ({ lastWinner, id }) => lastWinner ? (id === 'a' ? 'blue' : 'red') : 'white' };
  margin : 0 0.5em;
`

class Score extends React.Component{
  getPlayerScore(player){
    const { results, getMatchWinner } = this.props
    return results.filter(match => getMatchWinner(match) === player).length
  }
  render(){
    const { results, getMatchWinner } = this.props
    const lastMatchWinner = results.length && getMatchWinner(results[ results.length - 1 ])
    return (
      <Container>
        <Points lastWinner={ lastMatchWinner === "a" } id="a">{ this.getPlayerScore('a') }</Points>
        -
        <Points lastWinner={ lastMatchWinner === "b" } id="b">{ this.getPlayerScore('b') }</Points>
      </Container>
    )
  }
}

export default Score