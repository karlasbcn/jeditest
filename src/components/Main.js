import React                  from 'react'
import { bindActionCreators } from 'redux'
import { connect }            from 'react-redux'
import styled                 from 'styled-components'

import * as actions           from '../redux/actions'
import Player                 from './Player'
import Match                  from './Match'
import Score                  from './Score'
import Winner                 from './Winner'
import Buttons                from './Buttons'
import BackgroundImg          from '../images/starwars.jpg'

const Body = styled.div`
  @import url(https://fonts.googleapis.com/css?family=Lato:400,300);
  font-family: Lato;
  display: flex;
  flex-direction: column;
  overflow: hidden;
  color: white;
  background-image : url(${BackgroundImg});
  background-size: cover;
  position: absolute;
  height: 100%;
  width: 100%;
`

const Feedback = styled.div`
  text-align: center;
`

const TopBottom = styled.div`
  height: 120px;
  width: 100%;
`

const Container = styled.div`
  height: inherit;
  display: flex;
  flex-direction: row;
  justify-content : space-around;
  margin: 0 auto;
  width: 90%;
  & > div {
    height: 100%;
  }
`

const InnerCentral = styled.div`
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content : space-between;
  align-items: center;
  width: 500px;
`

const Vs = styled.div`
  text-align: center;
  font-size: 46px;
  margin-top: 24px;
`

class Main extends React.Component{
  componentDidMount(){
    const { fetchPeopleAndVehicles, preparePlayers } = this.props
    fetchPeopleAndVehicles().then(preparePlayers)
  }
  resetPlayers(){
    const { resetPlayers, preparePlayers } = this.props
    resetPlayers()
    preparePlayers()
  }
  getMatchWinner(match){
    const { a, b } = match
    if (a.time < b.time){
      return 'a'
    }
    if (a.time > b.time){
      return 'b'
    }
    return false
  }
  renderFetching(text){
    return (
      <Body>
        <Feedback>{ text }</Feedback>
      </Body>
    )
  }
  render(){
    const { people, players, match, playMatch, prepareMatch, results, matchDone } = this.props
    if (!people){
      return this.renderFetching('Fetching people and vehicles...')
    }
    if (!players){
      return this.renderFetching('Fetching player species...');
    }
    const lastMatch  = matchDone && results[results.length - 1];
    const lastWinner = lastMatch && this.getMatchWinner(lastMatch);
    return (
      <Body>
        <TopBottom>
          { matchDone && <Winner lastWinner={ lastWinner } players={ players } /> }
        </TopBottom>
        <Container>
          <Player id="1" player={ players.a } lastMatch={ lastMatch.a } />
          <InnerCentral>
            <Vs>VS</Vs>
            <Score getMatchWinner={ match => this.getMatchWinner(match) } results={ results } />        
            <Match { ...match } />
          </InnerCentral>
          <Player id="2" player={ players.b } lastMatch={ lastMatch.b } />
        </Container>
        <TopBottom>
          <Buttons 
            clickAction={ matchDone ? prepareMatch : playMatch } 
            clickReset={ () => this.resetPlayers() } 
            textAction={ matchDone ? 'Next match' : 'Play' } 
          />
        </TopBottom>
      </Body>
    )
  }
}

export default connect(state => state, dispatch => bindActionCreators(actions, dispatch))(Main)