import { get } from 'axios';

const fetch = (path, appendBase = true) => new Promise(function(resolve, reject){
  get((appendBase ? 'https://swapi.co/api' : '') + path)
    .then(response => resolve(response.data))
    .catch(reject);
});

export const fetchPeople = () => new Promise(function(resolve, reject){
  let people = [];
  const fetchPage = (url, firstPage = false) => (firstPage ? fetch('/people') : fetch(url, false))
    .then(data => {
      const { next, results } = data;
      people = [ ...people, ...results ];
      return next ? fetchPage(next) : resolve(people); 
    })
    .catch(reject);
  fetchPage('/people', true);
});

export const fetchVehicle = id => fetch('/vehicles/' + id);
export const fetchSpecie  = id => fetch('/species/' + id);