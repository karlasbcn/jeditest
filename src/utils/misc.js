export const getIdFromUrl = url => {
  const array = url.split('/').filter(chunk => !!chunk);
  return array[array.length - 1];
}

export const getRandomInt = (min, max) => {
  return Math.floor((Math.random() * ((max + 1) - min)) + min)
}

export const getRandomItem = (array, exclude = false) => {
  if (exclude){
    array = array.filter(item => JSON.stringify(item) !== JSON.stringify(exclude));
  }
  return array[ getRandomInt(0, array.length - 1) ];
}