import * as api                      from '../utils/api'
import { getIdFromUrl, getRandomInt, 
         getRandomItem }             from '../utils/misc'

export const ADD_PEOPLE_AND_VEHICLES = 'ADD_PEOPLE_AND_VEHICLES'
export const ADD_SPECIE              = 'ADD_SPECIE'
export const ADD_PLAYERS             = 'ADD_PLAYERS'
export const RESET_PLAYERS           = 'RESET_PLAYERS'
export const PREPARE_MATCH           = 'PREPARE_MATCH';
export const ADD_MATCH_RESULT        = 'ADD_MATCH_RESULT';

const addPeopleAndVehicles = (people, vehicles) => ({ type : ADD_PEOPLE_AND_VEHICLES, people, vehicles })
const addSpecie      = (id, specie) => ({ type : ADD_SPECIE, id, specie })
const addPlayers     = (a, b) => ({ type : ADD_PLAYERS, a, b })
const addMatchResult = matchResult => ({ type : ADD_MATCH_RESULT, matchResult })

export const resetPlayers = () => ({ type : RESET_PLAYERS })

export const prepareMatch = () => ({
  type     : PREPARE_MATCH,
  gold     : getRandomInt(100, 9999),
  distance : getRandomInt(1000, 99999)
})

export const fetchPeopleAndVehicles = function(){
  return function(dispatch){

    return new Promise(function(resolve){
      api.fetchPeople().then(people => {
        people = people
          .filter(character => character.vehicles.length)  //Just characters with vehicles to play
          .map(character => ({
            name     : character.name,
            specie   : getIdFromUrl(character.species[0]),
            vehicles : character.vehicles.map(getIdFromUrl)  
          }))
        const vehicleIds = people.reduce((prev, character) => {
          return [ ...prev, ...character.vehicles.filter(id => !prev.includes(id)) ]
        }, [])
        //Fetch all vehicles from the players, to exclude ones whith no speed or capacity
        Promise.all(vehicleIds.map(api.fetchVehicle)).then(results => {
          const vehicles = results.reduce((prev, result, index) => {
            const { name, max_atmosphering_speed, cargo_capacity } = result
            const vehicleId = vehicleIds[index]
            const vehicle = { 
              name, 
              speed    : parseInt(max_atmosphering_speed, 10), 
              capacity : parseInt(cargo_capacity, 10) 
            }
            if (!(vehicle.speed && vehicle.capacity)){
              return prev
            }
            return { ...prev, [ vehicleId ] : vehicle }
          }, {})
          const validVehicleIds = Object.keys(vehicles)
          //Discard characters with invalid vehicles
          people = people.reduce((prev, character) => {
            const characterVehicleIds = character.vehicles.filter(id => validVehicleIds.includes(id))
            if (!characterVehicleIds.length){
              return prev
            }
            return [ ...prev, { ...character, vehicles : characterVehicleIds }]
          }, [])
          dispatch(addPeopleAndVehicles(people, vehicles));
          resolve()
        })
      })
    })
  }
}

export const preparePlayers = function(){
  return function(dispatch, getState){
    const { people, species, vehicles } = getState()
    const playerA   = getRandomItem(people)
    const playerB   = getRandomItem(people, playerA)
    const vehicleA  = vehicles[getRandomItem(playerA.vehicles)]
    const vehicleB  = vehicles[getRandomItem(playerB.vehicles)]
    const getSpecie = id => new Promise(function(resolve){
      const specie = species[id]
      !!specie ? resolve(specie) : api.fetchSpecie(id).then(result => {
        const specie = result.name
        dispatch(addSpecie(id, specie))
        resolve(specie)
      })
    })
    getSpecie(playerA.specie).then(specieA => getSpecie(playerB.specie).then(specieB => {
      const getPlayer = (name, vehicle, specie) => ({ name, vehicle, specie })
      dispatch(addPlayers(getPlayer(playerA.name, vehicleA, specieA), getPlayer(playerB.name, vehicleB, specieB)))
      dispatch(prepareMatch())
    }))
  }
}

export const playMatch = function(){
  return function(dispatch, getState){
    const { match, players } = getState()
    const { gold, distance } = match;
    const matchResult = ['a', 'b'].reduce((prev, player) => {
      const { speed, capacity } = players[player].vehicle;
      const trips       = Math.ceil(gold / capacity);
      const timePerTrip = distance / speed;
      /* Every trip with cargo inside adds 2 hours =>  GOING =  (trips * (timePerTrip + 2))
         On every unload we have to come back, except for the last one => RETURNING : ((trips - 1) * timePerTrip)
      */
      const time = (trips * (timePerTrip + 2)) + ((trips - 1) * timePerTrip);
      return { ...prev, [ player ] : { time, trips } }
    }, {});
    dispatch(addMatchResult(matchResult));
  }
}