import { ADD_PEOPLE_AND_VEHICLES, ADD_SPECIE, 
         ADD_PLAYERS, RESET_PLAYERS,
         PREPARE_MATCH, ADD_MATCH_RESULT } from './actions';

const initialState = {
  people    : null,
  players   : null,
  species   : {},
  vehicles  : null,
  match     : null,
  results   : [],
  matchDone : false
}

const reducer = function(state = initialState, action) {
  switch (action.type) {
    case ADD_PEOPLE_AND_VEHICLES:
      return { 
        ...state, 
        people   : action.people,
        vehicles : action.vehicles
      }
    case ADD_SPECIE:
      return { ...state, species : { ...state.species, [action.id] : action.specie } }
    case ADD_PLAYERS:
      return { ...state, players : { a : action.a, b : action.b } }
    case RESET_PLAYERS:
      return { 
        ...state, 
        players : null,
        matchDone : false,
        results : []
      }
    case PREPARE_MATCH:
      return { 
        ...state, 
        match     : { gold : action.gold, distance : action.distance },
        matchDone : false
      }
    case ADD_MATCH_RESULT:
      return { 
        ...state, 
        results : [ ...state.results, action.matchResult ],
        matchDone : true
      }
    default:
      return state
  }
}

export default reducer